<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Test.dev</title>

    <!-- Bootstrap core CSS -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="/views/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/views/css/css.css" rel="stylesheet">
    <script src="/js/jquery-3.2.1.min.js"></script>
    <!--    <script src="/js/jquery-3.2.1.slim.min.js"></script>-->
    <script src="/js/ajsx-sort.js" type="text/javascript"></script>
</head>

<body>

<div class="navbar navbar-dark bg-dark">
    <div class="container d-flex justify-content-between">
        <a href="#" class="navbar-brand">My_Site</a>
    </div>
</div>

<section class="jumbotron text-center">
    <div class="container">
        <?php if (empty($data)):?>
        <h1><?php echo 'Акаунт приватний';?></h1>
        <?php endif;?>
        <h1><?php if (is_string($data)) echo '404';?></h1>
        <h1 class="jumbotron-heading">User: <?php  if(isset($data[0]->user->username)) echo $data[0]->user->username; ?></h1>
        <p class="lead text-muted"> Full Name: <?php if (isset($data[0]->user->full_name)) echo $data[0]->user->full_name ?>  </p>
        <img src="<?php if (isset($data[0]->user->profile_picture)) echo $data[0]->user->profile_picture; ?>" alt="Card image cap">
        <div><input id="sort" type="button" value="Sort" name="sort" data-sort="abc" class="btn btn-primary"></div>
    </div>
</section>

<div class="album text-muted">

</div>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
        <p>Max &copy; 2017</p>

    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/js/popper.min.js"></script>
<script src="/js/holder.min.js"></script>
<script>
    $(function () {
        Holder.addTheme("thumb", {background: "#55595c", foreground: "#eceeef", text: "Thumbnail"});
    });
</script>
<script src="/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>