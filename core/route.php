<?php

/*
Class-router to determine the requested page.
clings controllers classes;
*/

class Route
{

    static function start()
    {   /*
            Creates instances of page controllers and invokes the actions of these controllers.
        */
        // controller and default action
        $controller_name = 'IndexController';
        $action_name = 'action_views';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // we attach the file with the model class (there can be no model file)
        $controller_file = $controller_name . '.php';

        $controller_path = "./controllers/" . $controller_file;
        if (file_exists($controller_path)) {
            include "./controllers/" . $controller_file;
        } else {
            //redirect to error 404
            Route::ErrorPage404();
        }

        // create a controller
        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            if (isset($routes[1]) && !empty($routes[1]) && $routes[1]) {
                $controller->$action($routes[1]);
            } else {
                echo 'Введіть акаунт (test.dev/account) ';
            }

        } else {
            //redirect to error 404
            Route::ErrorPage404();
        }
    }

    public function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}