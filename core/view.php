<?php

class View
{
    function generate($template_view, $data = null)
    {   /*
        dynamically connect a common template (view),
        inside which will be built in
        to display the content of the page.
        */
        include './views/' . $template_view;
    }
}
