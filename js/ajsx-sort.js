$(document).ready(function () {
    $.ajax({
        type: 'POST',
        data: {sort: 'abc'},
        success: function (data) {
            $('.album').html(data);
        },
        error: function () {
            alert('error');
        }
    });
    $('#sort').click(function () {
        if ($('#sort').attr("data-sort") == 'abc') {
            $('#sort').attr("data-sort", "cba");
        }
        else if ($('#sort').attr("data-sort") == 'cba') {
            $('#sort').attr("data-sort", "abc");
        }
        $.ajax({
            type: 'POST',
            data: {sort: $('#sort').attr("data-sort")},
            success: function (data) {
                $('.album').html(data);
            },
            error: function () {
                alert('error');
            }
        });
    });
});
