<?php

// connect kernel files
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';

Route::start(); // run the router