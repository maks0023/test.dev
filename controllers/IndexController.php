<?php

require_once './core/controller.php';


class IndexController extends Controller
{/*   Get the username to work with the instagram api
     *
     *   @param string accountName
     */
    private $data;

    public function action_views($accountName)
    {
        $json = curl_init("https://www.instagram.com/" . $accountName . "/media/");
        curl_setopt($json, CURLOPT_RETURNTRANSFER,1);
        $this->data = curl_exec($json);
        if (isset(json_decode($this->data)->items)) $this->data = json_decode($this->data)->items;
        curl_close($json);
        $this->action_sort();
    }

    public function action_sort()
    {
        if (isset($_POST['sort']) && $_POST['sort'] == 'cba') {
            usort($this->data, function ($f1, $f2) {
                if ($f1->created_time < $f2->created_time) return -1;
                elseif ($f1->created_time > $f2->created_time) return 1;
                else return 0;
            });
            $this->view->generate('content.php', $this->data);
            return true;
        } elseif (isset($_POST['sort']) && $_POST['sort'] == 'abc') {
            $this->view->generate('content.php', $this->data);
            return true;
        }
        $this->view->generate('index.php', $this->data);
    }
}